﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomForest
{
    internal class GiniIndex : INorm<double>
    {
        #region INorm<double> Members

        public double Calculate(double[] v)
        {
            return v.Sum(p => p * (1 - p));
        }

        #endregion
    }
}
