﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomForest
{
    class RandomForest
    {
        // параметры почти те же самые, добавился один
        private INorm<double> _norm = null;
        private int _minLeafDataCount = 1;
        private int[] _trainingFeaturesSubset = null;
        private int _randomSubsetSize = 0; //zero if all features needed
        private double _maxProbability = 0;
        private int _maxDepth = Int32.MaxValue;
        private bool _showLog = false;
        private int _forestSize = 0;    
        private ConcurrentBag<ClassificationTree> _trees = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="norm">норма вектора вероятностей </param>
        /// <param name="forestSize">Количество деревьев</param>
        /// <param name="minLeafDataCount">минимально количество данных в наборе данных терминальной ноды</param>
        /// <param name="trainingFeaturesSubset">подмножество фичь которые могут участвовать в сплиттинге узла</param>
        /// <param name="randomSubsetSize">размер случайновыбираемых фич, как кандидатов при разделении ноды</param>
        /// <param name="maxProbability">максимальная вероятность класса, после достижения которой разделение ноды не происходит</param>
        /// <param name="maxDepth">максимальная глубина ноды</param>
        /// <param name="showLog">Показывать ли логи</param>
        public RandomForest(INorm<double> norm, int forestSize, int minLeafDataCount, int[] trainingFeaturesSubset = null,
            int randomSubsetSize = 0, double maxProbability = 0.95, int maxDepth = Int32.MaxValue,
            bool showLog = false)
        {
            _norm = norm;
            _minLeafDataCount = minLeafDataCount;
            _trainingFeaturesSubset = trainingFeaturesSubset;
            _randomSubsetSize = randomSubsetSize;
            _maxProbability = maxProbability;
            _maxDepth = maxDepth;
            _forestSize = forestSize;
            _showLog = showLog;
        }

        public void Train(IList<DataItem<double>> data)
        {
            if (_showLog)
            {
                Logger.Instance.Log("Training is started");
            }

            // Расспаралелвание обучения
            _trees = new ConcurrentBag<ClassificationTree>();
            Parallel.For(0, _forestSize, i =>
            {
                ClassificationTree ct = new ClassificationTree(
                    _norm,
                    _minLeafDataCount,
                    _trainingFeaturesSubset,
                    _randomSubsetSize,
                    _maxProbability,
                    _maxDepth,
                    false
                    );
                ct.Train(BasicStatFunctions.Sample(data, data.Count, true));
                _trees.Add(ct);
                if (_showLog)
                {
                    Logger.Instance.Log("Training of tree #" + _trees.Count + " is completed!");
                }
            });
        }

        // bagging
        public IDictionary<double, double> Classify(double[] v)
        {
            IDictionary<double, double> p = new Dictionary<double, double>();
            foreach (ClassificationTree ct in _trees)
            {
                IDictionary<double, double> tr = ct.Classify(v);
                double winClass = tr.First().Key;
                if (!p.ContainsKey(winClass))
                {
                    p.Add(winClass, 0);
                }
                p[winClass]++;
            }
            double denominator = p.Sum(x => x.Value);
            return
                p.ToDictionary(x => x.Key, x => x.Value / denominator)
                    .OrderByDescending(x => x.Value)
                    .ToDictionary(x => x.Key, x => x.Value);
        }

        public IList<ClassificationTree> Forest
        {
            get
            {
                return _trees.ToList();
            }
        }
    }
}
