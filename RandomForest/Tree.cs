﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomForest
{
    class Tree<T>
    {
        public Tree<T> Parent { get; set; }
        public Tree<T> Left { get; set; }
        public Tree<T> Right { get; set; }
        public T Data { get; set; }

        public Tree()
        {
            
        }

        public Tree(T data)
        {
            Data = data;
        }


        public bool IsLeaf
        {
            get
            {
                return ((Left == null) && (Right == null));
            }
        }

        public int Depth
        {
            get
            {
                int d = 0;
                Tree<T> node = this;
                while (node.Parent != null)
                {
                    d++;
                    node = node.Parent;
                }
                return d;
            }
        }
    }
}
