﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomForest
{
    class Logger
    {
        private static Logger _instance = null;
        public static Logger Instance
        {
            get {
                if (_instance == null)
                    _instance = new Logger();

                return _instance;
            }

            private set { }
        }

        private Logger() { }

        public void Log(string log)
        {
            Console.WriteLine(log);
        }

    }
}
