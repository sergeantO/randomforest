﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomForest
{
    class Program
    {
        static void Main(string[] args)
        {
            RandomForest crf = new RandomForest(
                new GiniIndex(),
                10,
                1,
                null,
                Convert.ToInt32(Math.Round(Math.Sqrt(ds.TrainSet.First().Input.Length))),
                0.95,
                1000,
                true
            );

            crf.Train(ds.TrainSet);
        }
    }
}
