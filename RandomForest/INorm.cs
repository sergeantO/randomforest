﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomForest
{
    interface INorm<T>
    {
        double Calculate(T[] v);
    }
}
