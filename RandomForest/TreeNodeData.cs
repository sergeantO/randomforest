﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomForest
{
    class TreeNodeData
    {
        // вектор вероятностей <id класса, его вероятность>
        internal IDictionary<int, double> Probabilities { get; set; }

        // стоимость данного узла, используется для того, что бы разбиение не было дороже текущей стоимости
        // чуть ниже мы представим это как норму вектора
        internal double Cost { get; set; }

        // предикат который будет использоваться для принятия решения, полностью излишний элемент, 
        // добавлен только ради эстетики, ведь мы не преследуем цель написать очень быстрый и экономный вариант -)
        //internal Predicate<double[]> Predicate { get; set; }

        // данные ассоциированные с текущей нодой, используется только во время обучения дерева
        internal IList<DataItem<double>> DataSet { get; set; }

        // индекс признака который минимизирует стоимость при разбиении
        internal int FeatureIndex { get; set; }

        // значение порога из области определения признака
        internal double FeatureValue { get; set; }
    }
}
