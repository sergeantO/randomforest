﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomForest
{
    class ClassificationTree
    {
        private Tree<ClassificationTree> _rootNode = null;
        private INorm<double> _norm = null;     // норма вектора вероятностей 
        private int _minLeafDataCount = 1;    // минимально количество данных в наборе данных терминальной ноды
        private int[] _trainingFeaturesSubset = null;    // подмножество фичь которые могут участвовать в сплиттинге узла 
        private int _randomSubsetSize = 0;    // размер случайновыбираемых фич, как кандидатов при разделении ноды
        private Random _random = null;
        private double _maxProbability = 0;     // максимальная вероятность класса, после достижения которой разделение ноды не происходит
        private int _maxDepth = Int32.MaxValue;     // максимальная глубина ноды
        private bool _showLog = false;
        private int _featuresCount = 0;


        public ClassificationTree(INorm<double> norm, int minLeafDataCount, int[] trainingFeaturesSubset = null,
            int randomSubsetSize = 0, double maxProbability = 0.95, int maxDepth = Int32.MaxValue,
            bool showLog = false)
        {
            _norm = norm;
            _minLeafDataCount = minLeafDataCount;
            _trainingFeaturesSubset = trainingFeaturesSubset;
            _randomSubsetSize = randomSubsetSize;
            _maxProbability = maxProbability;
            _maxDepth = maxDepth;
            _showLog = showLog;
        }

        public Tree<ClassificationTree> RootNode
        {
            get
            {
                return _rootNode;
            }
        }

        // обучение дерева
        public void Train(IList<DataItem<double>> data)
        {
            _featuresCount = data.First().Input.Length;
            if (_randomSubsetSize > 0)
            {
                _random = new Random(Helper.GetSeed());
            }
            IDictionary<double, double> rootProbs = ComputeLabelsProbabilities(data);
            _rootNode = new Tree<ClassificationTree>(new ClassificationTree()
            {
                DataSet = data,
                Probabilities = rootProbs,
                Cost = _norm.Calculate(rootProbs.Select(x => x.Value).ToArray())
            });

            // не люблю рекурсии   
            Queue<Tree<ClassificationTree>> queue = new Queue<Tree<ClassificationTree>>();
            queue.Enqueue(_rootNode);
            while (queue.Count > 0)
            {
                if (_showLog)
                {
                    Logger.Instance.Log("Tree training: queue size is " + queue.Count);
                }
                Tree<ClassificationTree> node = queue.Dequeue();
                int sourceCount = node.Data.DataSet.Count;

                // разделение ноды
                TrainNode(node, node.Data.DataSet, _trainingFeaturesSubset, _randomSubsetSize);
                if (_showLog && node.Childs.Count() > 0)
                {
                    Logger.Instance.Log("Tree training: source " + sourceCount + " is splitted into " +
                                    node.Childs.First().Data.DataSet.Count + " and " +
                                    node.Childs.Last().Data.DataSet.Count);
                }

                // проверка остановки и продолжение роста дерева
                foreach (Tree<ClassificationTreeData> child in node.Childs)
                {
                    if (child.Data.Probabilities.Count == 1 ||
                        child.Data.DataSet.Count <= _minLeafDataCount ||
                        child.Data.Probabilities.First().Value > _maxProbability ||
                        child.Depth >= _maxDepth)
                    {
                        child.Data.DataSet = null;
                        continue;
                    }
                    queue.Enqueue(child);
                }
            }


        }

        // разделение ноды
        private void TrainNode(Tree<ClassificationTree> node, IList<DataItem<double>> data, int[] featuresSubset, int randomSubsetSize)
        {
            // argmin нормы
            double minCost = node.Data.Cost;
            int idx = -1;
            double threshold = 0;
            IDictionary<double, double> minLeftProbs = null;
            IDictionary<double, double> minRightProbs = null;
            IList<DataItem<double>> minLeft = null;
            IList<DataItem<double>> minRight = null;
            double minLeftCost = 0;
            double minRightCost = 0;

            // если требуется случайное подмножество фич, то заполняе
            if (randomSubsetSize > 0)
            {
                featuresSubset = new int[randomSubsetSize];
                IList<int> candidates = new List<int>();
                for (int i = 0; i < _featuresCount; i++)
                {
                    candidates.Add(i);
                }
                for (int i = 0; i < randomSubsetSize; i++)
                {
                    int idxRandom = _random.Next(0, candidates.Count);
                    featuresSubset[i] = candidates[idxRandom];
                    candidates.RemoveAt(idxRandom);
                }
            }
            else if (featuresSubset == null)
            {
                featuresSubset = new int[data.First().Input.Length];
                for (int i = 0; i < data.First().Input.Length; i++)
                {
                    featuresSubset[i] = i;
                }
            }

            // пробегаемся по выбранным признакам
            foreach (int i in featuresSubset)
            {
                IList<double> domain = data.Select(x => x.Input[i]).Distinct().ToList();

                // и ищем порог для минимизации стоимости разбиения
                foreach (double t in domain)
                {
                    IList<DataItem<double>> left = new List<DataItem<double>>(); // подмножество обучающего множества левого потомка
                    IList<DataItem<double>> right = new List<DataItem<double>>(); // ну и правого
                    IDictionary<double, double> leftProbs = new Dictionary<double, double>(); // вектор вероятностей классов в подмножестве
                    IDictionary<double, double> rightProbs = new Dictionary<double, double>();
                    foreach (DataItem<double> di in data)
                    {
                        if (di.Input[i] < t)
                        {
                            left.Add(di);
                            if (!leftProbs.ContainsKey(di.Output[0]))
                            {
                                leftProbs.Add(di.Output[0], 0);
                            }
                            leftProbs[di.Output[0]]++;
                        }
                        else
                        {
                            right.Add(di);
                            if (!rightProbs.ContainsKey(di.Output[0]))
                            {
                                rightProbs.Add(di.Output[0], 0);
                            }
                            rightProbs[di.Output[0]]++;
                        }
                    }
                    if (right.Count == 0 || left.Count == 0)
                    {
                        continue;
                    }

                    // нормализация вероятностей 
                    leftProbs = leftProbs.ToDictionary(x => x.Key, x => x.Value / left.Count);
                    rightProbs = rightProbs.ToDictionary(x => x.Key, x => x.Value / right.Count);
                    double leftCost = _norm.Calculate(leftProbs.Select(x => x.Value).ToArray());    // вычисление стоимости разбиения                           
                    double rightCost = _norm.Calculate(rightProbs.Select(x => x.Value).ToArray());
                    double avgCost = (leftCost + rightCost) / 2;    // средняя стоимость разбиения
                    if (avgCost < minCost)
                    {
                        minCost = avgCost;
                        idx = i;
                        threshold = t;
                        minLeftProbs = leftProbs;
                        minRightProbs = rightProbs;
                        minLeft = left;
                        minRight = right;
                        minLeftCost = leftCost;
                        minRightCost = rightCost;
                    }
                }
            }

            // заполняем данные для текущей ноды и создаем потомков
            node.Data.DataSet = null;
            if (idx != -1)
            {
                //node should be splitted
                node.Data.Predicate = v => v[idx] < threshold;    // предикат который будет использоваться при принятии решений
                node.Data.Name = "x[" + idx + "] < " + threshold;
                node.Data.Probabilities = null;
                node.Data.FeatureIndex = idx;
                node.Data.FeatureValue = threshold;
                node.Left = new ClassificationTree()
                {
                    Probabilities = minLeftProbs.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value),
                    DataSet = minLeft,
                    Cost = minLeftCost
                };
                node.AddChild(new ClassificationTreeData()
                {
                    Probabilities = minRightProbs.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value),
                    DataSet = minRight,
                    Cost = minRightCost
                });
            }
        }

        // вычисление вероятностей классов в множестве, применяется в режиме классификации
        private IDictionary<double, double> ComputeLabelsProbabilities(IList<DataItem<double>> data)
        {
            IDictionary<double, double> p = new Dictionary<double, double>();
            double denominator = data.Count;
            foreach (double label in data.Select(x => x.Output[0]).Distinct())
            {
                p.Add(label, data.Where(x => x.Output[0] == label).Count() / denominator);
            }
            return p;
        }

        // классификация вхожного образа
        public IDictionary<double, double> Classify(double[] v)
        {
            Tree<ClassificationTree> node = _rootNode;
            while (!node.IsLeaf)
            {
                node = node.Data.Predicate(v) ? node.Childs.First() : node.Childs.Last();
            }
            return node.Data.Probabilities;
        }
    }
}
